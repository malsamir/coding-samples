#!/usr/bin/python
#Mohssen Alsamiri
#V1.0 1/29/18
#v2.0 2/13/19 Added Prometheus 
#v2.1 4/18/19 Added Average Ping Time
#v2.2 8/27/19 Added Destination Site for each IP

#Runs every 10 minutes
#Takes about 1 minute and 30 Seconfs to run
#The IPs, the type of IP, total ping count, and successful ping count  are sent are collect and put into Prometheus. 
#If the number of failed pings exceeds 5% an alert will be sent. 
#Prometheus and AlertManager handle the paging and email alerts. 
import os,sys,subprocess
import csv
from subprocess import Popen, PIPE, STDOUT
import random
import argparse
sys.path.append("REDACTED")
from NaServer import *
import threading
import shlex
import json
from shutil import copyfile
##Definitions###

#Flags: Debug for all output ; Verbose for change output 
parser = argparse.ArgumentParser()
parser.add_argument('-d', '--debug', action='store_true', dest='debug',
                    help='print debug messages')
parser.add_argument('-v', '--verbose', action='store_true', dest='verbose',
                    help='print verbose output')
parser.add_argument('-c', '--count', type=int,default=50,dest='count',
                    help='Number of packets to send: Default 50')
parser.add_argument('-e', '--exclude',dest='exclude',
                    help="IPs, Lifs, Nodes, or Clusters to exlude from the monitor: Default List '-bar','-1','-2'")
result = parser.parse_args()

#Prometheus Data Colection
outfile = "/opt/adm/WAN_Monitor.py.prom"
prodfile = "/opt/prometheus/node_exporter/textfile/WAN_Monitor.py.prom"

#Add REDACTED Pools, SC pools go in the left list and OR pools in the right. If you want a pool to be scanned on its own, add another list into this
pools=[['REDACTED'],['REDACTED']]
#Number of ping sent per IP ( Default 40) 
ping_count = result.count
#Dictionaries for destination ip and ip type 
dest_ip={}
ip_type={}
ip_dest={}
ping_time={}
#Exclusion List for REDACTED Filters
exclude_list = ['-bar','-1','-2','old','eol']
if(result.exclude):
    exclude = exclude_list + result.exclude.split(',')
else:
    exclude = exclude_list
######################
#get_switch
#Gets the network switch IP per cluster to be pinged
######################
def get_switch(server):
    swlist=[]
        tag = "";
        while tag != None:
                if not tag:
                        api_result = server.invoke('net-device-discovery-get-iter', 'max-records', 10000)
                else:
                        api_result = server.invoke('net-device-discovery-get-iter', 'tag', tag, 'max-records', 10000)

        if (api_result.results_errno() != 0):
                        reason = api_result.results_reason()
            if(result.verbose or result.debug):
                            print( reason + "\n" )
            if (not result.debug):
                                print ("API: Call failed for net-device-discovery-get-iter" + " " + reason + "\n")
            sys.exit(2)
                if api_result.child_get_int('num-records') == 0:
                        print( "No switch IPs returned" )
            if(not result.debug):
                            print("No switch IPs returned \n")
            return

                tag = api_result.child_get_string('next-tag')

                for device in api_result.child_get('attributes-list').children_get():
                        switch = device.child_get_string('discovered-device')
            if("REDACTED.com" in switch):
                            switch_ip = device.child_get('device-ip').child_get_string('ip-address')
                                swlist.append(switch_ip)
            if("sc11" in switch):
                ip_dest[switch_ip.decode("utf-8")]={"destination_site":"sc"}
            elif("jf4" in switch):
                ip_dest[switch_ip.decode("utf-8")]={"destination_site":"or"}


    return swlist

####################
#get_lifs
#Gets the data lifs to be pinged
##################
def get_lif(server):
    global ip_type
        liflist=[]
        tag = "";
        while tag != None:
                if not tag:
                        api_result = server.invoke('net-interface-get-iter', 'max-records', 10000)
                else:
                        api_result = server.invoke('net-interface-get-iter', 'tag', tag, 'max-records', 10000)

        if (api_result.results_errno() != 0):
                                reason = api_result.results_reason()
                        if(result.verbose or result.debug):
                                print( reason + "\n" )
                        if (not result.debug):
                                print("API: Call failed for net-interface-get-iter" + " " + reason + "\n")
            sys.exit(2)
                if api_result.child_get_int('num-records') == 0:
                        print( "No lifs returned" )
            if(not result.debug):
                            print("No lifs returned \n")
                        return

                tag = api_result.child_get_string('next-tag')

                for net in api_result.child_get('attributes-list').children_get():
                        lif_name = net.child_get_string('interface-name')
                        ip_addr = net.child_get_string('address')
                        role = net.child_get_string('role')
            if("data" in role.lower()):
                if not any( x in lif_name for x in exclude):
                                        liflist.append(ip_addr)
                    ip_type[ip_addr.decode("utf-8")]={"type":"lif"}
            if("REDACTED" in lif_name):
                ip_dest[ip_addr.decode("utf-8")]={"destination_site":"or"}
            elif("REDACTED" in lif_name):
                ip_dest[ip_addr.decode("utf-8")]={"destination_site":"sc"}
    return liflist

#################
#NBhost
#Takes a list of REDACTED pools to gather hosts/IPs. It collects hosts that are not not locked or disconnected. 
################
def NBhost(pools,pinglist):
        for pool in pools:
                cur_pool=pool
                fields="ip,host"
                process = Popen(["/usr/REDACTED/bin/REDACTED", "workstation","--target","%s" %(cur_pool),"--format", "script", "--fields","%s" %(fields),"classes=~'REDACTED' && status != 'disc' && status!='locked' && status !='unknown'"], stdout=PIPE, stderr=PIPE)
                (nb,err) = process.communicate()
                encoding = 'ascii'
                output = nb.decode(encoding)
        for REDACTED in  output.splitlines():
            host=REDACTED.split(',')[0]
            hostname=REDACTED.split(',')[1]
            pinglist.append(host)
            if("REDACTED" in hostname):
                ip_dest[host.decode("utf-8")]={"destination_site":"or"}
            elif("REDACTED" in hostname):
                ip_dest[host.decode("utf-8")]={"destination_site":"sc"}

        return pinglist
######################
#get_simple_cmd_output
#Helps simplify the shell command input and output
#
######################
def get_simple_cmd_output(cmd):
    args = shlex.split(cmd)
    return Popen(args, stdout=PIPE, stderr=STDOUT, shell=False).communicate()[0]

######################
#Pinger 
#This class is used to run fping and threading
#####################
class Pinger(object):
    status = {'good': [], 'bad': []} # Populated while we are running
    hosts = []
    # List of all hosts/ips in our input queue
    # How many ping process at the time.
    thread_count = 4
    # Lock object to keep track the threads in loops, where it can potentially be race conditions.
    lock = threading.Lock()

    def ping(self, ip):
    cmd = "/usr/sbin/fping -p 650 -c "+ str(ping_count) + " -q {host}".format(host=" ".join(ip))
        for x in get_simple_cmd_output(cmd).strip().split(' : ', 0) :
                lines = x.split("\n")
        for line in lines:
            ping_percent = (line.split(",")[0]).split("/")[-1]
            success_count = (line.split(",")[0]).split("/")[-2]
            avg_ping = float((line.split(",")[1]).split("/")[-2])
            split_ip = line.split(" ")[0]
            dest_ip[split_ip]={"ip":split_ip,"success_count":success_count}
            ping_time[split_ip]={"ping_time":avg_ping}
    return split_ip

    def pop_queue(self):
        ip = None
        self.lock.acquire() # Grab or wait+grab the lock.
        if self.hosts:
            ip = self.hosts.pop()
        self.lock.release() # Release the lock, so another thread could grab it.

        return ip

    def dequeue(self):
        while True:
            ip = self.pop_queue()
            if not ip:
                return None
            result = 'good' if (len(self.ping(ip)) == 0) else 'bad'
            self.status[result].append(ip)

    def start(self):
        threads = []
        for i in range(self.thread_count):
            # Create self.thread_count number of threads that together will
            # cooperate removing every ip in the list. Each thread will do the
            # job as fast as it can.
            t = threading.Thread(target=self.dequeue)
            t.start()
            threads.append(t)
        # Wait until all the threads are done. .join() is blocking.
        [ t.join() for t in threads ]
        return self.status

######################
#REDACTED
#Set up REDACTED login through REDACTED keys/certificates, used to run the API calls
##################
def REDACTED_login(clusters):
    transport = "HTTPS"
        port = 443
        style = "CERTIFICATE"
        cert = "/root/REDACTED-api/REDACTED-api.pem"
        key = "/root/REDACTED-api/REDACTED-api.key"
    server = NaServer(clusters, 1, 110)
        server.set_transport_type(transport)
        server.set_port(port)
        server.set_style(style)
        server.set_server_cert_verification(0)
        server.set_client_cert_and_key(cert, key)
    return server

#####################
#get_cluster
#Get a list of clusters from REDACTED
####################    
def get_cluster():
    filer = Popen(['/usr/REDACTED/bin/REDACTED','fileservers','--fields','controlstation','--format','scr'], stdout=PIPE)
        (out,err) = filer.communicate()
        cluster_list = str.split(out)
    return cluster_list

########################
#enter_prometheus
#Creates a .prom file that enters total packet count and successful packets sent
########################
def enter_prometheus(list):
    wan_help="#HELP Total Number of Packets Sent and Successful Number of Packets Sent  \n"
    wan_total="#TYPE wan_count_total gauge\n"
    wan_success="#TYPE wan_count_success gauge\n"
    wan_avg_ping_time="#TYPE wan_avg_ping_time_seconds gauge\n"
    for ip in list:
        wan_total+='wan_packet_count_total{destinationip="%s",type="%s",destinationsite="%s"} %s\n'% (ip,ip_type[ip]["type"],ip_dest[ip]["destination_site"],ping_count)
        wan_success+='wan_packet_count_success{destinationip="%s",type="%s",destinationsite="%s"} %s\n'%(ip,ip_type[ip]["type"],ip_dest[ip]["destination_site"],dest_ip[ip]["success_count"])
        wan_avg_ping_time+='wan_avg_ping_time_seconds{destinationip="%s",destinationsite="%s"} %s\n'%(ip,ip_dest[ip]["destination_site"],ping_time[ip]["ping_time"])
    if (result.debug or result.verbose):
            print (wan_help+wan_total+wan_success+wan_avg_ping_time)
    if(not result.debug):
        with open(outfile,"w") as f:
            f.write(wan_help+wan_total+wan_success+wan_avg_ping_time)

###################
#rm_duplicate
#Use Dictionary to get unique values from list
##################
def rm_duplicate(x):
  return list(dict.fromkeys(x))

#######
#MAIN
#######
def main():
    ip_list=[]
    cluster_list = get_cluster()
    global ip_type
    for clusters in cluster_list:
        if(result.debug):
            print clusters
        server = REDACTED_login(clusters)
                sw = get_switch(server)
                switches = set(sw) # Removes duplicate switch IPs
        for sw_ip in switches:
            ip_type[sw_ip.decode("utf-8")]={"type":"switch"}
        lifs = get_lif(server)
                ip_list = list(switches) + lifs + ip_list
    for pool in pools:
                nblist = []
                nb_list = NBhost(pool,nblist)
        for nbip in nb_list:
            ip_type[nbip]={"type":"batch"}
        ip_list = nb_list + ip_list
    if (result.debug):
        for ip in ip_list:
            print "Pinged: " + ip
    ping = Pinger()
        ping.thread_count = 16
    ip_list = rm_duplicate(ip_list)
    n = len(ip_list)/ping.thread_count
    lists =[ip_list[i:i + n] for i in xrange(0, len(ip_list), n)] #Creates a list of lists to do the fping in batchs, list divided by thread count. 
    ping.hosts = lists
    ping.start()
    enter_prometheus(ip_list)
    copyfile(outfile,prodfile)

if __name__ == "__main__":
    main()

